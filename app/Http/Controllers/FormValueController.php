<?php

namespace App\Http\Controllers;

use App\DataTables\FormValuesDataTable;
use App\Models\Form;
use App\Models\FormValue;
use App\Models\UserForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class FormValueController extends Controller
{

    public function index(FormValuesDataTable $dataTable)
    {
        if (\Auth::user()->can('manage-submitted-form')) {
            $forms = Form::all();
            return $dataTable->render('form_value.index', compact('forms'));
        } else {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function showSubmitedForms($form_id, FormValuesDataTable $dataTable)
    {
        if (\Auth::user()->can('manage-submitted-form')) {
            $forms = Form::all();
            $forms_details = Form::find($form_id);
            return $dataTable->render('form_value.view_submited_form', compact('forms', 'forms_details'));
        } else {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function show($id)
    {
        if (\Auth::user()->can('show-submitted-form')) {
            $form_value = FormValue::find($id);
            $array = json_decode($form_value->json);
            return view('form_value.view', compact('form_value', 'array'));
        } else {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function edit($id)
    {
        $usr = \Auth::user();
        $user_role = $usr->roles->first()->id;
        $form_value = FormValue::find($id);
        $formallowededit = UserForm::where('role_id', $user_role)->where('form_id', $form_value->form_id)->count();
        if (\Auth::user()->can('edit-form') && $usr->type == 'Admin') {


            $array = json_decode($form_value->json);
            $form = $form_value->Form;
            $frm = Form::find($form_value->form_id);
            return view('form.fill', compact('form', 'form_value', 'array'));
        } else {
            if (\Auth::user()->can('edit-form') && $formallowededit > 0) {
                $form_value = FormValue::find($id);
                $array = json_decode($form_value->json);
                $form = $form_value->Form;
                $frm = Form::find($form_value->form_id);
                return view('form.fill', compact('form', 'form_value', 'array'));
            } else {
                return redirect()->back()->with('error', __('Permission Denied.'));
            }
        }
    }

    public function destroy($id)
    {
        if (\Auth::user()->can('delete-submitted-form')) {
            FormValue::find($id)->delete();
            return redirect()->back()->with('success',  __('Form successfully deleted!'));
        } else {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function download_pdf($id)
    {
        $form_value = FormValue::where('id', 'REGEXP', '[[:<:]]' . $id . '[[:>:]]')->first();
        if ($form_value) {
            $form_value->createPDF();
        } else {
            $form_value = FormValue::where('id', '=', $id)->first();
            if (!$form_value) {

                $id = Crypt::decryptString($id);
                $form_value = FormValue::find($id);
            }
            if ($form_value) {
                $form_value->createPDF();
            } else {
                return redirect()->route('home')->with('error', __('File is not exist.'));
            }
        }
    }


    public function export(Request $request)
    {

        // dd($request->all());
        $form_values = FormValue::where('form_id', $request->form_id);
        if ($request->start_date && $request->end_date) {
            $form_values->whereBetween('form_values.created_at', [$request->start_date, $request->end_date]);
        }
        $form_values = $form_values->get();
        // dd($form_values);
        // $form_values = FormValue::whereIn('id',$ids)->get();
        foreach ($form_values as $form_value) {
            $ids[] = $form_value->id;
        }

        // $ids = json_decode($ids);
        if ($ids) {
            $form_values = FormValue::whereIn('id', $ids)->get();
            if ($form_values) {

                $file_path = 'app/public/csv/Temp_' . time() . '.csv';
                $file = fopen(storage_path($file_path), 'w');

                foreach ($form_values as $key => $form_value) {

                    $ValuForm_array = json_decode($form_value->json);

                    $csv_label = [];
                    $csv_value = [];


                    $skip = 0;
                    foreach ($ValuForm_array as $data) {
                        if ($skip) {
                            $skip--;
                            continue;
                        }
                        if (isset($data->value) || isset($data->values)) {

                            if ($data->type == "starRating") {
                                $csv_label[] = $data->label;
                                $csv_value[] = $data->value;
                            } else if (isset($data->values)) {

                                $value = '';
                                foreach ($data->values as $sub_data) {

                                    if ($data->type == "checkbox-group") {
                                        if (isset($sub_data->selected)) {
                                            $value .= $sub_data->label . ', ';
                                        }
                                    } else if ($data->type == "radio-group") {
                                        if (isset($sub_data->selected)) {
                                            $value .= $sub_data->label . ', ';
                                        }
                                    } else {
                                        if (isset($sub_data->selected)) {
                                            $value .= $sub_data->label . ', ';
                                        }
                                    }
                                }
                                $value = rtrim($value, ', ');
                                $csv_label[] = $data->label;
                                $csv_value[] = $value;
                            } else {
                                if ($data->type == "file") {
                                } else {
                                    if (isset($data->subtype) && $data->subtype == 'signature') {
                                    } else {
                                        $csv_label[] = $data->label;
                                        $csv_value[] = $data->value;
                                    }
                                }
                            }
                        } else if ($data->type == "header") {
                            if (isset($data->selected) && $data->selected) {
                                $skip = intval($data->number_of_control);
                                $csv_label[] = $data->label;
                                $csv_value[] = 'N/A';
                            } else {
                                $csv_label[] = $data->label;
                                $csv_value[] = '';
                            }
                        } else {
                            if (isset($data->label)) {

                                $csv_label[] = ($data->label) ? $data->label : '';
                                $csv_value[] = '';
                            }
                        }
                    }

                    if ($key == 0) {
                        fputcsv($file, $csv_label);
                    }
                    fputcsv($file, $csv_value);
                }
                // dd($file_path,$form_value->Form->title ,$file);
                fclose($file);
                $headers = array(
                    'Content-Type: text/csv',
                  );

                return response()->download(storage_path($file_path), $form_value->Form->title . '_' . ".csv",$headers);
            } else {
                return redirect()->back()->with('error', __('Permission Denied.'));
            }
        } else {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }
}
