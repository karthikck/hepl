@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(['route' => 'users.store', 'method' => 'Post', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="form-group col-6 ">
        {{ Form::label('name', __('Name')) }}
        <div class="input-group ">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-user"></i>
                </div>
            </div>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter First Name']) !!}
        </div>
    </div>
    <div class="form-group col-6 ">
        {{ Form::label('lastname', __('Last Name')) }}
        <div class="input-group ">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-user"></i>
                </div>
            </div>
            {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Enter Last Name']) !!}
        </div>
    </div>
    <div class="form-group col-6 ">
        {{ Form::label('mobempid', __('Mobile or Emp ID')) }}
        <div class="input-group ">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-user"></i>
                </div>
            </div>
            {!! Form::text('mobempid', null, ['class' => 'form-control', 'placeholder' => 'Enter Mobile or Emp ID']) !!}
        </div>
    </div>
    <div class="form-group col-6">
        {{ Form::label('email', __('Email')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-6">
        {{ Form::label('lob', __('LOB')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('lob', null, ['class' => 'form-control', 'placeholder' => 'Enter LOB']) !!}
        </div>
    </div>
 
    <div class="form-group col-6">
        {{ Form::label('department', __('Department')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('department', null, ['class' => 'form-control', 'placeholder' => 'Enter Depeartment']) !!}
        </div>
    </div>
</div>

<div class="row">
<div class="form-group col-6">
        {{ Form::label('subdepartment', __('Sub Department')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('subdepartment', null, ['class' => 'form-control', 'placeholder' => 'Enter Sub Department']) !!}
        </div>
    </div>
    <div class="form-group col-6">
        {{ Form::label('zone', __('Zone')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('zone', null, ['class' => 'form-control', 'placeholder' => 'Enter Zone']) !!}
        </div>
    </div>
</div>
<div class="row">
<div class="form-group col-6">
        {{ Form::label('designation', __('Designation')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('designation', null, ['class' => 'form-control', 'placeholder' => 'Enter Designation']) !!}
        </div>
    </div>
    <div class="form-group col-6">
        {{ Form::label('reportingoffice', __('Reporting Office')) }}
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            {!! Form::text('reportingoffice', null, ['class' => 'form-control', 'placeholder' => 'Enter Reporting Office']) !!}
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="form-group col-6">
        {{ Form::label('roles', __('Role')) }}
        <div class="input-group ">

            {!! Form::select('roles', $roles, null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div> -->
<div class="btn-flt">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
    <button type="sub" class="btn btn-primary">{{ __('Save') }}</button>
</div>
{!! Form::close() !!}
