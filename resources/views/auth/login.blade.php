@extends('layouts.app')
@section('title', 'Login')
@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h4>{{ __('Login') }}</h4>
        </div>

        <div class="card-body">
            <form method="POST" class="needs-validation" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <input class="form-control {{ __('Email') }}" placeholder="{{ __('E-Mail Address') }}"
                        type="email" name="email" id="email" value="{{ old('email') }}" onfocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback d-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="d-block">
                        <label for="password" class="control-label">{{ __('Password') }}</label>
                        <div class="float-right">
                            <a href="{{ route('password.request') }}" class="text-small">
                                {{ __('Forgot Password ?') }}
                            </a>
                        </div>
                    </div>
                    <input type="password" placeholder="{{ __('Password') }}"
                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                    @error('password')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" name="remember" value="{{ old('remember') }}"
                            id="customCheckLogin" type="checkbox">
                        <label class="custom-control-label" for="customCheckLogin">{{ __('Remember me') }}</label>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary my-4">{{ __('Sign In') }}</button>
                </div>
            </form>


        </div>
    </div>
    <div class="mt-5 text-muted text-center">
        {{ __('Dont have an account?') }} <a href="{{ route('register') }}">{{ __('Create new account') }}</a>
    </div>
    <div class="simple-footer">
        &copy; {{date('Y')}} <a href="#" class="font-weight-bold ml-1" target="_blank">{{config('app.name')}}</a>
    </div>
@endsection
